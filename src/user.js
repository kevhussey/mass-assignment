// User data model
const User = {
    name: "",
    email: ""
};

//passes test 1 & 2
Object.defineProperties(User, {
    id: {
        value: auth_user_id(),
        writable: false
    },
    role: {
        value: 'user',
        writable: false
    }
});
//passes test 3
Object.seal(User);

// A psudo authenticator
// returns authenticated user ID
function auth_user_id() {
    return 2;
}


module.exports = { User, auth_user_id };
